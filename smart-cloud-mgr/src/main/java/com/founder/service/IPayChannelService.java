package com.founder.service;

import com.founder.core.domain.PayChannel;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPayChannelService {

    PayChannel selectPayChannel(int id);

    PayChannel selectPayChannel(String channelId, String mchId);

    Page<PayChannel> selectPayChannelList(int offset, int limit, PayChannel payChannel);

    @Deprecated
    Integer count(PayChannel payChannel);

    @Deprecated
    List<PayChannel> getPayChannelList(int offset, int limit, PayChannel payChannel);

    int addPayChannel(PayChannel payChannel);

    int updatePayChannel(PayChannel payChannel);
}
